export * as IMAGES from './images';
export * as REGEX from './regex';

//credential
export const BASE_URL = import.meta.env.BASE_URL_API;

//storage
export const ACCESS_TOKEN_KEY = '__a_fab__';
