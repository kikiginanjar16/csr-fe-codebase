import ReactDOM from 'react-dom/client';
import './assets/style/global.css';
import { Routes } from '@generouted/react-router';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(<Routes />);
