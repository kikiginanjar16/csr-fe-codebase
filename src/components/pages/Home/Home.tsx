function Home() {
  return (
    <div className="w-full h-screen grid place-content-center text-blue-600">
      <h1 className="text-center text-5xl">Home</h1>
    </div>
  );
}

export default Home;
